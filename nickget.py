import time,hexchat

# Nickname Retention Script -- by perpetual_disbelief
# Put this in ~/.config/hexchat/addons for automagic loading (assuming *nix; should work cross-platform)

# Done: 
# * retains preferred nickname on multiple servers (including if you connect/disconnect during session)
# * submission of Nickserv auth after nick change (on servers using Nickserv)
# * monitors notify of preferred nick (and forcibly keeps preferred nick on notify)
# * automatic submission of nickserv ghost command 
# * sends regular ping commands to (hopefully) speed up detection of dead connection.

# Not done yet:
# * option to gag ping response 
# * support for recover/release nickserv command
# * extended testing
# * any kind of optimization whatsoever
# * support for non-Nickserv authorization


# Change these as desired / needed (be aware of flooding yourself off or making NickServ ignore you)
PING_ENABLED=False
SECONDS_BETWEEN_ATTEMPTS=30
SECONDS_BETWEEN_PINGS=120
SECONDS_AFTER_NICK=3
SECONDS_AFTER_GHOST=3


# Don't change anything else unless you speak Python
__module_name__="NickRetainer"
__module_description__="Automatically retake nickname"
__module_version__="0.1111"

nickget_lastattempt={}
nickget_ping_timer={}
nickget_serverList = []
mynick=hexchat.get_prefs("irc_nick1")



		
	
def nickget_keepnick(userdata):
	for chan in hexchat.get_list('channels'):
		conText=hexchat.find_context(channel=chan.channel)
		if conText is not None:
			Server=conText.get_info("server")
			if Server not in nickget_serverList and Server is not None:
				nickget_serverList.append(Server)
	for Server in nickget_serverList:
		if Server not in nickget_lastattempt:
			nickget_lastattempt[Server]=0
		if nickget_lastattempt[Server] > 0:
			nickget_lastattempt[Server]=nickget_lastattempt[Server]-1
		conText=hexchat.find_context(server=Server)
		if conText is not None:
			if PING_ENABLED:
				if Server not in nickget_ping_timer:
					nickget_ping_timer[Server]=0
				if nickget_ping_timer[Server]>0:
					nickget_ping_timer[Server]=nickget_ping_timer[Server]-1
				if nickget_ping_timer[Server]==0:
					nickget_ping_timer[Server]=SECONDS_BETWEEN_PINGS
					conText.command("ping")			
			currentnick=conText.get_info("nick")
			if hexchat.nickcmp(mynick,currentnick) != 0 and nickget_lastattempt[Server]==0:
				notifyList=[name.nick for name in hexchat.get_list("notify")]
				if mynick not in notifyList:
					conText.command("notify " + mynick)
					notifyList=[name.nick for name in hexchat.get_list("notify")]
				################################################################################################################
				# weird glitch patch 9/12/2015 for notifyList throwing AttributeError on name.nick.  will investigate eventually.
				# Log for later reference:
				# AttributeError: 'str' object has no attribute 'nick'
				# (but normally all entries of notifyList are objects with attribute nick? )
				################################################################################################################
				try:
					myStatus=[name.flags for name in notifyList if name.nick==mynick][0]
				except AttributeError:
					myStatus=0					
				sekrit=conText.get_info("nickserv")
				if myStatus==0:
					nickget_lastattempt[Server]=SECONDS_BETWEEN_ATTEMPTS
					conText.command("nick " + mynick)
					if sekrit is not None:
						time.sleep(SECONDS_AFTER_NICK)
						conText.command("nickserv identify " + sekrit)
				else:
					if sekrit is not None:
						conText.command("nickserv ghost " + mynick + sekrit)
						time.sleep(SECONDS_AFTER_GHOST)


		else:
			nickget_serverList.remove(Server)
	return True

hexchat.hook_timer(1000,nickget_keepnick)

