# hexchat-nickname-retention

A Python script for the HexChat IRC client.

Automatically change nicknames when you lose your preferred nickname.

Automatically sends NickServ GHOST commands if your nickname is still
online

Automatically sends NickServ IDENTIFY command after changing your nickname.

Optional disabled-by-default feature: send regular pings to server.
(KNOWN ISSUE: If this feature is enabled, the frequency of pings may increase after reconnects unless script is reloaded...)


To install (*nix instructions; Windows users, drag and drop the file to your HexChat addons dir, wherever that is):

cp getnick.py ~/.config/hexchat/addons

To run:
    Will run automatically at HexChat startup
    To start/stop/restart manually, use /py (load/unload/reload) getnick.py

For more information on features / TODO list:
	Read comments in getnick.py